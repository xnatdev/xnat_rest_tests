package org.nrg.testing.dicom;

public class AnonConstants {

    public static final String ANON2_IMAGE_TYPE = "ORIGINAL\\PRIMARY\\PROTON_DENSITY\\NONE";
    public static final String ANON2_HASHED_SOP_INSTANCE_UID = "2.25.18971457369541893112737536732177973003";
    public static final int FAKE_DICOM_TAG = 0x38720018; // for various tests, we want to see how DicomEdit handles a tag it doesn't know about

}
