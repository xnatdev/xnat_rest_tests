package org.nrg.testing.xnat.customforms.pojo;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class AppliesTo {
    private String entityId;
    private String idCustomVariableFormAppliesTo;
    private String status;
}
