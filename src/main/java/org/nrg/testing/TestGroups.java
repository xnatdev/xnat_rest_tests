package org.nrg.testing;

public class TestGroups {

    public static final String ALIAS_TOKENS = "alias-tokens";
    public static final String ARCHIVE = "archive";
    public static final String ANONYMIZATION = "anonymization";
    public static final String AUTHENTICATION = "authentication";
    public static final String CONFIG_SERVICE = "config-service";
    public static final String CONTAINERS = "containers";
    public static final String CUSTOM_VARIABLES = "custom-variables";
    public static final String DICOM_SCP = "dicom-scp";
    public static final String DICOM_ROUTING = "dicom-routing";
    public static final String DIRECT_ARCHIVE = "direct-archive";
    public static final String EVENT_SERVICE = "event-service";
    public static final String FILE_MITIGATION = "file-mitigation";
    public static final String IMPORTER = "importer";
    public static final String INVESTIGATORS = "investigators";
    public static final String MERGE = "merge";
    public static final String METADATA_EXTRACTION = "metadata-extraction";
    public static final String OPEN_XNAT = "open-xnat";
    public static final String ORCHESTRATION = "orchestration";
    public static final String PERMISSIONS = "permissions";
    public static final String PREARCHIVE = "prearchive";
    public static final String PREFERENCES = "preferences";
    public static final String RESOURCES = "resources";
    public static final String SCANS = "scans";
    public static final String SCHEDULED_EVENTS = "scheduled-events";
    public static final String SCHEMAS = "schemas";
    public static final String SEARCH = "search";
    public static final String SHARING = "sharing";
    public static final String SMOKE = "smoke";
    public static final String USERS = "users";
    public static final String VALIDATION = "validation";
    public static final String WORKFLOWS = "workflows";
    public static final String XML = "xml";
    public static final String CUSTOM_FORMS = "custom_forms";

}
